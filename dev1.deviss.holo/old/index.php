<?php include_once __DIR__ . "/main.php"; // Get location of all files
?>
<!DOCTYPE html>
<html lang="FR-fr">

<head>
  <?php include_once __DIR__ . '/front_vue/head.php'; ?>
  <?php echo printHead(); ?>


</head>

<body class='bg-light text-secondary'>
  <?php echo printHeader(); ?>    

  <main>



    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1 class="text-center">Erreur 404</h1>
          <h2 class="text-center">La page demandée n'existe pas.</h2>
        </div>
      </div>
    </div> 
  </main>





  <?php echo printFooter(); ?>

</body>

</html>
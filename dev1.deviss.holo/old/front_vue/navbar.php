<?php
include_once __DIR__."/../main.php";
function pageBelongToMenu($pageLink,$inMenuLink){ // Checking if the page belongs to the menu
    $retour=false;
    if(basename($pageLink)==basename($inMenuLink)){
        $retour=true;
    }
    return $retour;
}
function printNavbar(){ //  Printing the navbar
    $pageLink=basename($_SERVER["PHP_SELF"]);
    $retour="";
    $navbarConf=json_decode(file_get_contents(DATA."navbar.json"),true); // Getting the navbar configuration
    $retour.=" 
        <nav class='navbar navbar-expand-lg navbar-dark bg-primary' id='navbarVit'>
            <!-- <style>@media all and (min-width:992px){.dropdown-menu li{position:relative}.dropdown-menu .submenu{display:none;position:absolute;left:100%;top:-7px}.dropdown-menu .submenu-left{right:100%;left:auto}.dropdown-menu>li:hover{background-color:#f1f1f1}.dropdown-menu>li:hover>.submenu{display:block}}@media (max-width:991px){.dropdown-menu .dropdown-menu{margin-left:.7rem;margin-right:.7rem;margin-bottom:.5rem}}</style> -->
            <div class='container-fluid'>
                <a class='navbar-brand' href='#'>Menu</a>
                <button class='navbar-toggler' type='button' data-bs-toggle='collapse' data-bs-target='#main_nav' aria-expanded='false' aria-label='Toggle navigation'>
                    <span class='navbar-toggler-icon'></span>
                </button>
                <div class='collapse navbar-collapse' id='main_nav'>


                    <ul class='navbar-nav'>
        "; // Opening the navbar
    foreach($navbarConf as $menuName => $menuContent){ // For each menu
        $belongToMenu=false;
        $menuHTML="";
        $menuLink=array_shift($menuContent); // Getting the link of the menu
        $submenuHTML="";
        foreach($menuContent as $submenuName => $submenuContent){ // For each submenu
            
            $submenuLink=array_shift($submenuContent);
            if(pageBelongToMenu($pageLink,$submenuLink)){
                $belongToMenu=true;
            }else{
                $submenuHTML.="<li><a class='dropdown-item' href='$submenuLink'> $submenuName </a></li>";
            }

        }
        if(pageBelongToMenu($pageLink,$menuLink)){ // If the page belongs to the menu
            $belongToMenu=true;
        }

        $active="";
        if($belongToMenu){
            $active="active";
        }
        if(count($menuContent)>0){
            $menuHTML.="<li class='nav-item dropdown'>
            <a class='nav-link dropdown-toggle $active' href='$menuLink' data-bs-toggle='dropdown'> $menuName </a>
            <ul class='dropdown-menu'>";
            $menuHTML.=$submenuHTML;
            $menuHTML.="</ul>
            </li>";
        }else {
            $menuHTML.="<li class='nav-item'><a class='nav-link $active' href='$menuLink'> $menuName </a></li>";
        }
        $retour.=$menuHTML;
    }
    $retour.="</ul>
    </div>
    </div>
    </nav>
    ";
    return $retour;
}
function generateNavbar($filename){
    global $pathToModel;
    file_put_contents($pathToModel.$filename,printNavbar());
}
?>

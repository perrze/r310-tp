<?php
// Ionic icons : https://ionic.io/ionicons
include_once __DIR__."/../main.php";
/* --------------------------- Controller include_once --------------------------- */
include_once __DIR__."/header.php";
include_once __DIR__."/footer.php";


function printHtmlImport(){ // Importing all the css and js files
    return "
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'> 
    
    
    <link href='/css/bootstrap.min.css' rel='stylesheet'>".
    // <link href='/css/surcharge.css' rel='stylesheet'>
    
    "";
}

function titleDef(){ // Defining the title of the page
    global $title;
    $title="";
    $titleFound=false;
    $listTitle=json_decode(file_get_contents(DATA."title.json"),true); // Getting the list of titles
    foreach($listTitle as $titre => $link){
        if(basename($_SERVER["PHP_SELF"])==$link){ // If the current page is the one in the list
            $title=$titre;
            $titleFound=true;
            break;
        }
    }
    if(!$titleFound){ // If the current page is not in the list
        $title="Non défini";
    }
    return $title;
}

function printHead(){ // Printing the head of the page
    titleDef();
    global $title;
    $htmlTitle="<title>$title</title>";
    $htmlIco="<link rel='icon' type='image/png' href='/sources/logo/PetitLogo.ico'>";
    return printHtmlImport().$htmlTitle.$htmlIco; // Returning the head
}
?>
<?php
include_once __DIR__."/navbar.php";
function printHeader(){
    $retour="<header>";
    $retour.=printNavbar();
    $retour.="</header>".printJsImport();
    return $retour;
}
function printJsImport(){
    
    return "
    <script src='/js/bootstrap.bundle.min.js'></script>
    <script src='/js/jquery-3.6.0.min.js'></script>
    <script>
        function dechtml(str){
            return $('<textarea/>').html(str).text();
        }
    </script>
    <script src='/js/main.js'></script>
    ";
}

?>
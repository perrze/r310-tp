<?php
include_once __DIR__."/../main.php";
function generateFooter($pathToJSON){
    $footerConf=array();
    $footerConf=json_decode(file_get_contents($pathToJSON),true);
    // var_dump($footerConf);
    $retour="";

   /* It's creating the first part of the footer. */
    $retour.="
    <footer class='bg-dark text-light'>
        <div class='container'>
            <div class='row'>
                <div class='col-md-3 mb-md-0 mb-4'>
                    <h2 class='footer-heading'>Le Pilier Malouin</h2>
                    <p>Bienvenue chez nous !</p>
                </div>
                <div class='col-md-9'>
    ";
    if(count($footerConf)<0){ // If error reading (Doesn't happen)
        alert("Erreur de lecture du footer","danger",false);
    }else{
        /* It's calculating the number of columns that will be used. */
        $iteration=intdiv(count($footerConf),3)*3;
        $count=0;
        foreach($footerConf as $title => $array){
           /* It's creating a new row every 3 columns. */
            if($count%3==0){
                $retour.="
                    <div class='row'>
                ";
            }
            // var_dump($footerConf[$title]);
            
            /* It's creating an empty column if the title is empty. */
            if($title==""){
                // echo "test";
                $retour.="<div class='col-md-4 mb-md-0 mb-4'></div>";
            }else{
                // $link=array_shift($footerConf[$title]);
                // Taking first element which is lin is config file syntax 
                $link=array_shift($array);
                // var_dump($title);
                
                /* It's creating a column with a title and a list of links. */
                $retour.="
                <div class='col-md-4 mb-md-0 mb-4'>
                    <a href='$link' class='text-decoration-none text-reset'> <h2 class='footer-heading'>$title</h2></a>
                    <ul class='list-unstyled'>
                ";
                foreach($array as $subtitle => $link){
                    $retour.="
                        <li><a href='$link' class='d-block'>$subtitle</a></li>
                    ";
                }
                $retour.="
                    </ul>
                </div>
                ";
                /* It's closing the row if the number of columns is a multiple of 3. */
                if($count%3==0 && $count !=0){
                    $retour.="</div>";
                }
            }
            $count+=1;
        }
        /* It's adding empty columns if the number of columns is not a multiple of 3. */
        if($count<$iteration){
            while($count<$iteration){
                $count+=1;
                $retour.="<div class='col-md-4 mb-md-0 mb-4'></div>";
            }
            $retour.="</div>";
        }
    }  
    $retour.="
        </div>
        </div>
        <div class='row mt-5 pt-4 border-top'>
        <div class='col-md-6 col-lg-8 mb-md-0 mb-4'>
            <p class='copyright mb-0'>
            Copyright &copy;<script>
                document.write(new Date().getFullYear());
            </script> All rights reserved. | lepiliermalouin.bzh</a>
            </p>
        </div>
        <div class='col-md-6 col-lg-4 text-md-right'>
            <ul class='list-unstyled'>
                <li class=''><a href='http://intra.lepiliermalouin.bzh' class='text-decoration-none'><ion-icon name='business'></ion-icon>&nbsp;Intranet</a></li>
                <li class=''><a href='http://lepiliermalouin.bzh' class='text-decoration-none'><ion-icon name='home-outline'></ion-icon>&nbsp;Site vitrine</a></li>
            </ul>
        </div>
    </div>
</div>
</div>
</footer>
";
    return $retour;
}
    file_put_contents(__DIR__."/footer.html",generateFooter(DATA."footer.json"));
    echo generateFooter(DATA."footer.json");


<?php 

// NON FONCTIONNEL
require_once(__DIR__."/main.php");
$mysqli=connectionDB();

if(isset($_GET["TitreArticle"])){
    $arrayEntree=array("TitreArticle","Synopsis","ContenuArticle","NomJeu","PrixJeu","NoteJeu","NomCategorie","sent");
    $arraySortie=array();
    foreach($arrayEntree as $val){
        $_GET[$val]=filter_var($val,FILTER_SANITIZE_EMAIL);
    }
    createArticle($mysqli,1,$_GET["TitreArticle"],$_GET["ContenuArticle"],$_GET["Synopsis"],$_GET["NomJeu"],$_GET["PrixJeu"],$_GET["NoteJeu"],$_GET["NomCategorie"]);
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <?php getHead(); ?>
</head>
<body>
    <?php getHeader(); ?>
    <article>
    	<div class="jumbotron-fluid">
        
      </div>
    </article>
    <main>
        <div>
        <form action="/addArticle.php" method="get">
        <label for="TitreArticle">Titre de l'article:</label><br>
        <input type="text" id="TitreArticle" name="TitreArticle" value=""><br>
        <label for="Synopsis">Synopsis:</label><br>
        <textarea id="Synopsis" name="Synopsis" value=""></textarea><br><br>
        <label for="Synopsis">Contenu article:</label><br>
        <textarea id="ContenuArticle" name="ContenuArticle" value=""></textarea><br><br>
        <label for="NomJeu">Nom du jeu:</label><br>
        <input type="text" id="NomJeu" name="NomJeu" value=""><br>
        <label for="PrixJeu">Prix du Jeux:</label><br>
        <input type="text" id="PrixJeu" name="PrixJeu" value=""><br>
        <label for="NoteJeu">Note du jeu:</label><br>
        <input type="text" id="NoteJeu" name="NoteJeu" value=""><br>
        <label for="NomCategorie">Sélectionner une Catégorie:</label>
        <select id="NomCategorie" name="NomCategorie">
        <?php
            $listCat=getListCategorie($mysqli);
            
            foreach($listCat as $cat){
                echo "<option value=".$cat["NomCategorie"].">".$cat["NomCategorie"]."</option>";
            }
        ?>

        </select> 

        <input type="submit" id="sent" value="Envoyer">
        </form>
        </div>
        
    </main>
    <?php 
    	getFooter(); 
    ?>
</body>
</html>
<?php closeDB($mysqli); ?>
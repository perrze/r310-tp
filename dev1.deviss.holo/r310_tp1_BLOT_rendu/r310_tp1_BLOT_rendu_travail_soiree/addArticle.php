<?php
require_once(__DIR__."/main.php");
$mysqli=connectionDB();
// var_dump($_GET);
// var_dump(readDB($mysqli,"SELECT Id_Categorie FROM categorie WHERE NomCategorie = '".$_GET["NomCategorie"]."'")[0]["Id_Categorie"]);


if(isset($_GET["TitreArticle"])){
    $arrayEntree=array("TitreArticle","Synopsis","ContenuArticle","NomJeu","PrixJeu","NoteJeu","NomCategorie");
    $arraySortie=array();
    $ok=true;
    foreach($arrayEntree as $val){
        if($_GET[$val]==""){
            $ok=false;
        }
        $_GET[$val]=filter_var($_GET[$val],FILTER_SANITIZE_EMAIL);
    }
    if($ok){
        $_GET["PrixJeu"]=floatval($_GET["PrixJeu"]);
        $_GET["NoteJeu"]=floatval($_GET["NoteJeu"]);
        // var_dump($_GET["NomCategorie"]);
        echo createArticle($mysqli,1,$_GET["TitreArticle"],$_GET["ContenuArticle"],$_GET["Synopsis"],$_GET["NomJeu"],$_GET["PrixJeu"],$_GET["NoteJeu"],$_GET["NomCategorie"]);
    }
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <?php getHead(); ?>
    
</head>
<body>
    <?php getHeader(); ?>
    <?php getNav(); ?>
    <article>
    	<div class="jumbotron-fluid">
        
      </div>
    </article>
    <main class="mx-4 my-4">
        <div class="container">
        <form action="/addArticle.php" method="get">
        <label for="TitreArticle" class="form-label">Titre de l'article:</label><br>
        <input type="text" id="TitreArticle" name="TitreArticle" value="" class="form-control"><br>
        <label for="Synopsis" class="form-label">Synopsis:</label><br>
        <textarea id="Synopsis" name="Synopsis" value="" class="form-control"></textarea><br><br>
        <label for="Synopsis" class="form-label">Contenu article:</label><br>
        <textarea id="ContenuArticle" name="ContenuArticle" value="" class="form-control"></textarea><br><br>
        <div class="row">
            <div class="col">
                <label for="NomJeu" class="form-label">Nom du jeu:</label><br>
                <input type="text" id="NomJeu" name="NomJeu" value="" class="form-control"><br>
            </div>
            <div class="col">
                <label for="NomCategorie" class="form-label">Sélectionner une Catégorie:</label>
                <select id="NomCategorie" name="NomCategorie" class="form-control">
                <?php
                    $listCat=getListCategorie($mysqli);
                    
                    foreach($listCat as $cat){
                        echo "<option value=".$cat["NomCategorie"].">".$cat["NomCategorie"]."</option>";
                    }
                ?>

                </select> 
            </div>
        </div>
        <div class="row">
            <div class="col">
                <label for="PrixJeu" class="form-label">Prix du Jeux:</label><br>
                <input type="text" id="PrixJeu" name="PrixJeu" value="" class="form-control"><br>
            </div>
            <div class="col">
                <label for="NoteJeu" class="form-label">Note du jeu:</label><br>
                <input type="text" id="NoteJeu" name="NoteJeu" value="" class="form-control"><br>
            </div>
        </div>
        
        

        <input type="submit" id="sent" value="Envoyer" class="btn btn-primary">
        </form>
        </div>
        
    </main>
    <?php 
    	getFooter(); 
    ?>
</body>
</html>
<?php closeDB($mysqli); ?>
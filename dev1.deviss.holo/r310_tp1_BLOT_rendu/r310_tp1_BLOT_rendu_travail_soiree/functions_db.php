<?php
function connectionDB(){
    $servername = "localhost";
    $user = "root";
    $password= "";
    $database="r310jeuxrt";

    $mysqli = new mysqli($servername, $user, $password, $database);
    
    if ($mysqli->connect_errno) {
        echo 'Connection error: ' . $mysqli->connect_errno;
    }
    $mysqli->set_charset("utf8");

    // echo $mysqli->host_info . "\n";
    return $mysqli;
}

function closeDB($mysqli){
    return $mysqli->close();

}

function readDB($mysqli,$sql_input){
    $result=$mysqli->query($sql_input);
    if($result==false){
        return false;
    }else{
        return $result->fetch_all(MYSQLI_ASSOC);
    }
}

function writeDB($mysqli,$sql_input){
    $result=$mysqli->query($sql_input);
    if($result==false){
        return false;
    }else{
        return $result;
    }
}

// connectionDB();
?>
<?php 

require(__DIR__."/main.php");

$Id_Article=$_GET["idArticle"];

$mysqli=connectionDB();
// var_dump(getArticleById($mysqli,1)[0][""]);
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <?php getHead(); ?>
    
</head>
<body>
    <?php getHeader(); ?>
    <?php getNav(); ?>
    <article>
    	<div class="jumbotron-fluid">
        
      </div>
    </article>
    <main class="container-fluid row">
        <div class="col-1"></div>
        <div class="col">
            <?php echo displayArticle($mysqli,$Id_Article); ?>
            <?php echo displayImages($mysqli,$Id_Article); ?>
            <?php echo displayComments($mysqli,$Id_Article); ?>
        </div>
        <div class="col-1"></div>
    </main>
    <?php 
    	getFooter(); 
    ?>
</body>
</html>
<?php closeDB($mysqli); ?>
<?php 
require(__DIR__."/main.php");
$mysqli=connectionDB();
// var_dump(createArticle($mysqli,1,"TitreTest","ContenuTest","SynopsisTest","NomJeu","10.99","10","RPG"));

$articlesTable=getListArticles($mysqli);
$enteteTable=array("TitreArticle","NomJeu","PrixJeu","NoteJeu","DateCreationArticle");

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <?php getHead(); ?>
</head>
<body>
    <?php getHeader(); ?>
    <?php getNav(); ?>
    <article>
    	<div class="jumbotron-fluid">
        
      </div>
    </article>
    <main>
        <?php echo displayTabArticles($enteteTable,$articlesTable); ?>
    </main>
    <?php 
    	getFooter(); 
    ?>
</body>
</html>
<?php closeDB($mysqli); ?>
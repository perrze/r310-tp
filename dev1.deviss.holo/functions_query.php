<?php
require_once __DIR__."/functions_db.php";
// Query informations about an article
function getListArticles($mysqli){
    return readDB($mysqli,"SELECT Id_Article, TitreArticle, NomJeu, PrixJeu, NoteJeu,DateCreationArticle FROM article;");
}
function getArticleById($mysqli,$id_article){
    return readDB($mysqli,"SELECT * FROM article WHERE id_article = $id_article;");
}
function getPathImageById($mysqli,$id_article){
    return readDB($mysqli,"SELECT * FROM `image` WHERE id_article = $id_article;");
}
function getListAvisbyId($mysqli,$id_article){
    return readDB($mysqli,"SELECT * FROM `avis` WHERE id_article = $id_article;");
}

function getListCategorie($mysqli){
    return readDB($mysqli,"SELECT NomCategorie FROM `categorie`");
}

function getAuthorArticle($mysqli,$id_article){
    $Id_Membre=readDB($mysqli,
    "SELECT Id_Membre FROM `rediger` WHERE Id_Article=$id_article;"
    )[0]["Id_Membre"];
    return readDB($mysqli,
        "SELECT Login FROM `membre` WHERE Id_Membre=$Id_Membre;"
    )[0]["Login"];
}
function getAuthorAvis($mysqli,$Id_Membre){
    return readDB($mysqli,
        "SELECT Login FROM `membre` WHERE Id_Membre=$Id_Membre;"
    )[0]["Login"];
}
function getUpdateDateArticle($mysqli,$id_article){
    return readDB($mysqli,
    "SELECT DateModificationArticle FROM `rediger` WHERE Id_Article=$id_article;"
    )[0]["DateModificationArticle"];
}

// Write a new Article to DB
function createArticle($mysqli,$Id_Membre,$TitreArticle,$ContenuArticle,$Synopsis,$NomJeu,$PrixJeu,$NoteJeu,$NomCategorie){
    // return $NomCategorie;
    
    writeDB($mysqli,
        "INSERT INTO `article`(TitreArticle,ContenuArticle,Synopsis,NomJeu,PrixJeu,NoteJeu)
        VALUES('$TitreArticle','$ContenuArticle','$Synopsis','$NomJeu','$PrixJeu','$NoteJeu');");
    $id_article=$mysqli->insert_id;
    writeDB($mysqli,"INSERT INTO `rediger`(Id_Article,Id_Membre)
        VALUES($id_article,$Id_Membre);");
    
    // Old foreach when categorie was an array
    // foreach($NomCategorie as $cat){
    //     $id_cat=readDB($mysqli,"SELECT Id_Categorie FROM categorie WHERE NomCategorie = '$cat'")[0]["Id_Categorie"]; // Get the id of this categorie
    //     writeDB($mysqli, "INSERT INTO `classer`(Id_Article,Id_Categorie)
    //         VALUES($id_article,$id_cat);");
    // }
    
    $id_cat=readDB($mysqli,"SELECT Id_Categorie FROM categorie WHERE NomCategorie = '$NomCategorie'")[0]["Id_Categorie"]; // Get the id of this categorie
        writeDB($mysqli, "INSERT INTO `classer`(Id_Article,Id_Categorie)
        VALUES($id_article,$id_cat);");
    // METHODE POUR RECUP DERNIER ID : mysqli -> insert_id()  
    return $NomCategorie;  
}

function updateArticle($mysqli,$id_article,$id_membre,$data){
    foreach($data as $col => $cont){
        echo $col;
        writeDB($mysqli,"UPDATE `article` SET $col = '$cont' WHERE Id_Article = $id_article;");
    }
    // Pas d'update de la table rediger car problème de clé étrangère
    // writeDB($mysqli, "DELETE FROM `rediger`
    // WHERE Id_Article=$id_article AND Id_Membre=$id_membre;");

    // writeDB($mysqli,"INSERT INTO `rediger`(Id_Article,Id_Membre,DateModificationArticle)
    // VALUES($id_article,$id_membre,CURRENT_TIMESTAMP);");
}
// $mysqli=connectionDB();
// updateArticle($mysqli,12,4,array("TitreArticle"=>"Jeuisunbotitre"));

function deleteArticle($mysqli,$id_article){
    writeDB($mysqli,"DELETE FROM `avis` WHERE Id_Article=$id_article");
    writeDB($mysqli,"DELETE FROM `classer` WHERE Id_Article=$id_article");
    writeDB($mysqli,"DELETE FROM `rediger` WHERE Id_Article=$id_article");
    writeDB($mysqli,"DELETE FROM `article` WHERE Id_Article=$id_article");
    return true;
}


?>
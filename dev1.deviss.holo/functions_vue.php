<?php
function displayTabArticles($entete, $content)
{
    $tableau = "<table class='table'><tr>";

    foreach ($entete as $nomCol) {
        $tableau .= "<th>$nomCol</th>";
    }
    $tableau .= "
    <th>Show</th>
    <th>Update</th>
    <th>Delete</th>
    ";
    $tableau .= "</tr>";



    foreach ($content as $line) {
        $tableau .= "<tr>";
        foreach ($entete as $cellName) {
            $tableau .= "<td>" . $line[$cellName] . "</td>";
        }
        $Id_Article=$line["Id_Article"];
        $tableau .= <<<HTML
        <td> <a href="/game.php?idArticle=$Id_Article" class="text-decoration-none"><i class="bi bi-search"></i></i>  </a></td>
        <td> <i class="bi bi-arrow-clockwise"></i>  </td>
        <td> <a href="/removeArticle.php?Id_Article=$Id_Article" class="text-decoration-none"><i class="bi bi-trash3"></i></a> </td>
        HTML;
        $tableau .= "</tr>";
    }


    $tableau .= "</table>";
    return $tableau;
}

function displayArticle($mysqli, $Id_Article)
{
    // Call to functions_query and get info of an article
    $InfoArticle = getArticleById($mysqli, $Id_Article)[0];

    //Get simple var for all (To put into HTML)
    $titre = $InfoArticle["TitreArticle"];
    $synop = $InfoArticle["Synopsis"];
    $nomJeu = $InfoArticle["NomJeu"];
    $date = $InfoArticle["DateCreationArticle"];
    $note = $InfoArticle["NoteJeu"];
    $prix = $InfoArticle["PrixJeu"];
    $cont = $InfoArticle["ContenuArticle"];
    $author = getAuthorArticle($mysqli, $Id_Article); // Recupération auteur
    $dateUpdate = getUpdateDateArticle($mysqli, $Id_Article);

    $imageList=getPathImageById($mysqli,$Id_Article); // récup image pochette
    $couverture="";
    if(count($imageList)>0){
        $couverture=$imageList[0]["Chemin"];
    }
    if ($note > 7) {
        $color = "success";
    } else if ($note > 4) {
        $color = "warning";
    } else {
        $color = "danger";
    }

    $return = <<<HTML
        <h1 class="text-center">$titre</h1>
        <div class="text-center"><p class="fw-bold fst-italic"> <i class="bi bi-arrow-clockwise"></i></i> Publié le $dateUpdate par $author  </p></div>



        <div class="row my-2">
            <div class="col border border-2 rounded">
                <div class="row">
                    <div class="col-2 mx-3 my-3">
                        <img src="/$couverture" alt="Image de couverture" class="img-fluid ">
                    </div>
                    <div class="col">
                        <h3 class="text-start fw-bold">$nomJeu</h3>
                        <p class="text-start">Prix du jeu: $prix €</p>
                        <p class=" fs-6 fw-bold ">$synop</p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row my-4">
            <div class="col border border-2 rounded">
                <div class="col"><h4 class='text-start'><span class="fw-bold text-$color h3"><i class="bi bi-quote"></i></i>$note/10</span> Critique :</h4></div>
                <p>$cont</p>
            </div>
        </div>
            
    HTML;
    return $return;
}

function displayImages($mysqli, $Id_Article)
{
    $imageList=getPathImageById($mysqli,$Id_Article);
    if(count($imageList)>0){

    
        $couverture=$imageList[0]["Chemin"];
        unset($imageList[0]);
        $return=<<<HTML
            <div class="border border-2 rounded">
                <div class="my-3 mx-3">
                    <h3>Images du jeu </h3>
                    <div class="row">
                        <div class="col-4">
                            <img src="/$couverture" alt="Image de couverture" class="img-fluid ">
                        </div>
                        <div class="col">
                            <div class="row">
                            
        HTML;
        $perRow=0;
        foreach($imageList as $Image){
            if($perRow==0){
                $return.="<div class='row'>";
            }
            $return.="<div class='col-6'><img src=/".$Image["Chemin"]." alt='Aie' class='img-fluid'></div>";
            $perRow+=1;
            if($perRow==2){
                $perRow=0;
                $return.="</div>";
            }
        }
        $return.=<<<HTML
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        HTML;
        return $return;
    }
}

function displayComments($mysqli,$Id_Article){
    $commentList=getListAvisbyId($mysqli,$Id_Article);
    if(count($commentList)>0){

    
        $return=<<<HTML
            <div class="container-fluid col border border-2 rounded my-5">
                <h3 class="mx-3 my-3">Avis des Internautes</h3>      
        HTML;

        
        foreach($commentList as $comment){
            // $idAvis=$comment["Id_Avis"];
            $author=getAuthorAvis($mysqli,$comment["Id_Membre"]);
            $date=$comment["DateCreationAvis"];
            $note=$comment["NoteAvis"];
            $titre=$comment["TitreAvis"];
            $cont=$comment["TexteAvis"];
            if ($note > 7) {
                $color = "success";
            } else if ($note > 4) {
                $color = "warning";
            } else {
                $color = "danger";
            }
        
            $return.=<<<HTML
                <div class="container-fluid border border-2 rounded mx-3 my-3">
                    <p class="fw-bold">Avis de <span class="text-muted">$author</span>, le $date :</p>
                    <h6 class="fw-bold"><span class="text-$color"><i class="bi bi-quote"></i></i>$note/10</span>&#9;$titre</h6>
                    <p>$cont</p>

                </div>
            HTML;   
        }


        return $return;
    }
}


